#include "defines.hh"
#include "machine.hh"

CoffeeMachine machine;

void setup()
{
  Serial.begin(9601);

  machine = CoffeeMachine();
}

void loop() {
  //toggle();
  
  while (!Serial.available()) {} // wait for data
  
  if (Serial.available()) // do we got data ?
  {
    char cmd = Serial.read();
    switch (cmd)
    {
      case POWER_TOGGLE:
        machine.toggle_power();
        break;
      case ONE_CUP:
        machine.one_cup();
        break;
      case TWO_CUPS:
        machine.two_cups();
        break;
      default:
        break;
    }
  }
  
  delay(100);
}
