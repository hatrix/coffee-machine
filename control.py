from time import sleep
import readline
import serial

message = "p: Power toggle\n1: One cup\n2: Two cups\n → "

ser = serial.Serial('/dev/ttyACM0', 9601) # Establish the connection on a specific port
while True:
    choice = input(message)
    ser.write(choice.encode('ascii')) # Convert the decimal number to ASCII then send it to the Arduino
    print('Command sent!\n')
    sleep(.1)
