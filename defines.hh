#pragma once

#define MACHINE_PWR_TGL  13
#define MACHINE_ONE_CUP  12
#define MACHINE_TWO_CUPS 11

#define POWER_TOGGLE    'p'
#define ONE_CUP         'o'
#define TWO_CUPS        't'
