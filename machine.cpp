#include "machine.hh"

void CoffeeMachine::toggle_power()
{
  digitalWrite(MACHINE_PWR_TGL, HIGH);
  delay(100);
  digitalWrite(MACHINE_PWR_TGL, LOW);
}

void CoffeeMachine::one_cup()
{
  digitalWrite(MACHINE_ONE_CUP, HIGH);
  delay(100);
  digitalWrite(MACHINE_ONE_CUP, LOW);
}

void CoffeeMachine::two_cups()
{
  digitalWrite(MACHINE_TWO_CUPS, HIGH);
  delay(100);
  digitalWrite(MACHINE_TWO_CUPS, LOW);
}
