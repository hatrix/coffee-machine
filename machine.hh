#pragma once

#include "Arduino.h"
#include "defines.hh"

class CoffeeMachine
{
public:
  CoffeeMachine()
  {
    pinMode(MACHINE_PWR_TGL, OUTPUT);
    pinMode(MACHINE_ONE_CUP, OUTPUT);
    pinMode(MACHINE_TWO_CUPS, OUTPUT);
  }

  void toggle_power();
  void one_cup();
  void two_cups();
};
